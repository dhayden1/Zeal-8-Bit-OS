mainmenu "Zeal 8-bit OS Configuration"

    config CPU_FREQ
        int "CPU Clock Frequency (Hz)"
        default 7372800 if TARGET_RC2014
        default 10000000
        help
            Defines the CPU frequency in Hz, this may be used for counting time when no hardware timer
            is available.

    config TARGET
        string
        default "zeal8bit" if TARGET_ZEAL_8_BIT
        default "trs80" if TARGET_TRS_80
        default "agon" if TARGET_AGON_LIGHT
        default "rc2014" if TARGET_RC2014

    choice COMPILATION_TARGET
        prompt "Target board"
        help
            Choose the compilation target.

        config TARGET_ZEAL_8_BIT
            bool
            prompt "Zeal 8-bit computer"
            help
                Compile for Zeal 8-bit computer.

        config TARGET_TRS_80
            bool
            prompt "TRS-80 Model I"
            help
                Compile for TRS-80 Model I.

        config TARGET_AGON_LIGHT
            bool
            prompt "Agon Light"
            help
                Compile for Agon Light.

        config TARGET_RC2014
            bool
            prompt "RC2014 Pro with SIO/2"
            help
                Compile for RC2014Pro or equivalent with SIO/2 extension board.

    endchoice

    # Configuration options depending on the target
    if TARGET_ZEAL_8_BIT
        source "target/zeal8bit/Kconfig"
    endif

    if TARGET_TRS_80
        source "target/trs80/Kconfig"
    endif

    if TARGET_AGON_LIGHT
        source "target/agon/Kconfig"
    endif

    if TARGET_RC2014
        source "target/rc2014/Kconfig"
    endif

    source "kernel/Kconfig"
